function init_map (max_zoom, abovepages, layoutpages) {

var cell_width = 320,
    cell_height = 209;

if (abovepages === undefined) { abovepages = [] }
 
var map = L.map('map', {
        maxZoom: max_zoom || 9,
        minZoom: 0,
        zoom: 0,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});


var markers = [
  {
    "zoom": 2,
    "x": 156.3,
    "y": -23.0,
    "id": "t1"
  },
  {
    "zoom": 7,
    "x": 7.078,
    "y": -9.938,
    "id": "t13"
  },
  {
    "zoom": 6,
    "x": 312.891,
    "y": -10.016,
    "id": "t24"
  },
  {
    "zoom": 6,
    "x": 17.156,
    "y": -9.922,
    "id": "t25"
  },
  {
    "zoom": 6,
    "x": 25.078,
    "y": -9.891,
    "id": "t27"
  },
  {
    "zoom": 6,
    "x": 44.969,
    "y": -10.031,
    "id": "t30"
  },
  {
    "zoom": 6,
    "x": 52.719,
    "y": -10.094,
    "id": "t31"
  },
  {
    "zoom": 6,
    "x": 94.922,
    "y": -9.906,
    "id": "t34"
  },
  {
    "zoom": 6,
    "x": 117.063,
    "y": -9.859,
    "id": "t36"
  },
  {
    "zoom": 6,
    "x": 134.938,
    "y": -9.859,
    "id": "t37"
  },
  {
    "zoom": 6,
    "x": 137.359,
    "y": -9.859,
    "id": "t38"
  },
  {
    "zoom": 6,
    "x": 145.047,
    "y": -9.938,
    "id": "t39"
  },
  {
    "zoom": 6,
    "x": 155.0,
    "y": -9.859,
    "id": "t41"
  },
  {
    "zoom": 6,
    "x": 172.578,
    "y": -9.969,
    "id": "t42"
  },
  {
    "zoom": 6,
    "x": 176.906,
    "y": -9.953,
    "id": "t43"
  },
  {
    "zoom": 6,
    "x": 174.953,
    "y": -3.484,
    "id": "t44"
  },
  {
    "zoom": 6,
    "x": 185.156,
    "y": -10.063,
    "id": "t45"
  },
  {
    "zoom": 6,
    "x": 195.953,
    "y": -11.438,
    "id": "t47"
  },
  {
    "zoom": 6,
    "x": 204.969,
    "y": -10.188,
    "id": "t48"
  },
  {
    "zoom": 6,
    "x": 217.156,
    "y": -9.938,
    "id": "t50"
  },
  {
    "zoom": 6,
    "x": 223.094,
    "y": -9.922,
    "id": "t51"
  },
  {
    "zoom": 6,
    "x": 242.922,
    "y": -9.984,
    "id": "t52"
  },
  {
    "zoom": 6,
    "x": 246.875,
    "y": -9.984,
    "id": "t53"
  },
  {
    "zoom": 6,
    "x": 297.094,
    "y": -9.984,
    "id": "t54"
  },
  {
    "zoom": 6,
    "x": 302.813,
    "y": -9.875,
    "id": "t55"
  },
  {
    "zoom": 6,
    "x": 307.094,
    "y": -9.844,
    "id": "t56"
  },
  {
    "zoom": 9,
    "x": 308.0391,
    "y": -12.0,
    "id": "t57"
  },
  {
    "zoom": 6,
    "x": 313.219,
    "y": -10.328,
    "id": "t62"
  }
];
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers);

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);

var hash = new L.Hash(map);

function is_above_page (item) {
    var m = /^_DSC(\d+)\.png/.exec(item.name);
    if (m) {
        var pn = parseInt(m[1]),
            pni = abovepages.indexOf(pn);
        // console.log("AP", pn, pni);
        return (abovepages.indexOf(pn) != -1)
    }
}

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    // console.log("data", data);
    var pelts = [];
    var above_spread = false;
    var x = 0,
        y = 0,
        items = data['@graph'];
    for (var i=0, l=items.length; i<l; i++) {
        var item = items[i],
            m = /^_DSC(\d+)\.png/.exec(item.name),
            itemname = m ? m[1] : '';

        if (layoutpages && layoutpages[itemname]) {
            x = layoutpages[itemname].x;
            y = layoutpages[itemname].y;
            // console.log("using layoutpages, placing", itemname, "at", x, y);
            pelts.push({x: x, y: y, item: items[i]});
            continue
        }
        if (is_above_page(items[i])) {
            // above_spread = true;
            y = 0;
            x -= 1;
        } else {
            y = 1;
        }
        // pelts.push({x: i, y: 0, item: items[i]});
        // console.log("placing", items[i].name, "at", x, y);
        pelts.push({x: x, y: y, item: items[i]});
        x += 1;
        // if (i == 1) break;
    }
    var vt = leafygal.layout(pelts, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);

    map.addLayer(markers_layer);
    L.control.layers(null, { "Show links": markers_layer }).addTo(map);


  } else {
    console.log("server ERROR");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();


}
