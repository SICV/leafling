# Leafling

A collection of useful scripts, templates, and recipes for using the [leaflet](http://leafletjs.org) map paradigm to create (non-) map based interfaces based on tiled images. The scripts are a mixture of python (for producing image tiles), and javascript to be used in the browser along with leaflet.

The *legacy* folder contains earlier versions of scripts.

See also: <https://gitlab.constantvzw.org/aa/leafygal>


## Install Leaflet

There is a package.json file with leaflet as a dependency, which means you can use *npm* + the command:

    npm install

To install leaflet to the location that the templates expect. Or you could do it some other way :)


## Scripts
### tileimages-ld

Sample output for a single image:

```json
{
  "tilewidth": 256, 
  "tileheight": 256,
  "minzoom": 0, 
  "maxzoom": 3, 
  "images": [
    {
      "tiles": "tiles/00080_WereldculturenRMV_RV-A45-74.o.jpg/z{z}y{y}x{x}.jpg", 
      "id": "00080_WereldculturenRMV_RV-A45-74.o.jpg", 
      "name": "00080_WereldculturenRMV_RV-A45-74.o.jpg"
    }
  ]
}
```


## Cookbook

### Making a single image viewer

Rule tileimages on the image to produce the image.json file.
Combine the json with the singleimage template to produce the final HTML.

```bash
scripts/tileimages image.jpg > image.json
scripts/jinjafy --data image.json templates/singleimage.html > image.html
```

As a makefile:

```makefile
SCRIPTS=../scripts/

%.json: %.jpg
    $(SCRIPTS)tileimages $< > $@

%.html: %.jpg %.json ../templates/singleimage.html
    $(SCRIPTS)jinjafy --data $*.json ../templates/singleimage.html > $@
```

### Making an image gallery

As a makefile:
```
SCRIPTS=../scripts/

%.json: %.jpg
    $(SCRIPTS)tileimages $< > $@

%.html: %.jpg %.json ../templates/singleimage.html
    $(SCRIPTS)jinjafy --data $*.json ../templates/singleimage.html > $@
```
